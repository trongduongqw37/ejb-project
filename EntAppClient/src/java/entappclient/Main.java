/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package entappclient;

import dto.Account;
import ejb.AccountBeanRemote;

/**
 *
 * @author Admin
 */
public class Main {

    @javax.ejb.EJB
    private static AccountBeanRemote accountBean;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Account account = accountBean.findAccountById(1);
        System.out.println(account.toString());
    }
    
}
