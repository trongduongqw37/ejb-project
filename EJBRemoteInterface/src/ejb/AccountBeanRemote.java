/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/SessionRemote.java to edit this template
 */
package ejb;

import dto.Account;
import javax.ejb.Remote;

/**
 *
 * @author Admin
 */
@Remote
public interface AccountBeanRemote {

    Account findAccountById(int id);
}
