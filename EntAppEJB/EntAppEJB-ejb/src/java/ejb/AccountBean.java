/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package ejb;

import dto.Account;
import javax.ejb.Stateless;

/**
 *
 * @author Admin
 */
@Stateless
public class AccountBean implements AccountBeanRemote {
    
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

    @Override
    public Account findAccountById(int id) {
        return new Account(id, "Duong");
    }

}
